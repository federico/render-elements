extern crate structopt;
#[macro_use]
extern crate structopt_derive;

extern crate failure;
#[macro_use]
extern crate failure_derive;

extern crate cairo;
extern crate rsvg;

use failure::Error;
use rsvg::HandleExt;
use std::fs::File;
use std::io::{self, Read};
use std::path::PathBuf;
use std::process;
use structopt::StructOpt;

#[cfg_attr(rustfmt, rustfmt_skip)]
#[derive(StructOpt, Debug)]
#[structopt(name = "render-elements", about = "Render SVG elements to separate files.")]
struct Opt {
    #[structopt(short = "i",
                long = "input",
                help = "Input file",
                parse(from_os_str))]
    filename: PathBuf,

    #[structopt(help = "Element names")]
    elements: Vec<String>,

    #[structopt(long = "translated",
                help = "Whether to translate the images by a constant amount")]
    translated: bool,
}

#[derive(Debug, Fail)]
enum ProcessingError {
    #[fail(display = "Cairo error: {:?}", 0)]
    CairoError(cairo::Status),

    #[fail(display = "Rendering error")]
    RenderingError,

    #[fail(display = "Element not found: '{}'", 0)]
    ElementNotFound(String),
}

impl From<cairo::Status> for ProcessingError {
    fn from(status: cairo::Status) -> ProcessingError {
        ProcessingError::CairoError(status)
    }
}

const TRANSLATION_OFFSET: i32 = 47;

fn run(opt: &Opt) -> Result<(), Error> {
    let mut file = File::open(&opt.filename)?;

    let mut bytes = Vec::new();
    file.read_to_end(&mut bytes)?;

    let handle = rsvg::Handle::new_from_data(&bytes)?;
    let dimensions = handle.get_dimensions();

    check_if_elements_are_present(&handle, &opt.elements)?;

    let offset = if opt.translated {
        TRANSLATION_OFFSET
    } else {
            0
    };

    for id in &opt.elements {
        let (w, h) = (dimensions.width + offset, dimensions.height + offset);
        let surface = cairo::ImageSurface::create(cairo::Format::ARgb32, w, h)
            .map_err(ProcessingError::CairoError)?;

        let cr = cairo::Context::new(&surface);
        cr.translate(f64::from(offset), f64::from(offset));

        let frag = fragment(id);

        if !handle.render_cairo_sub(&cr, Some(frag.as_ref())) {
            eprintln!("Could not render element '{}'", id);
            Err(ProcessingError::RenderingError)?;
        }

        let filename = format!("{}.png", id);
        let mut out_file = File::create(&filename)?;

        surface.write_to_png(&mut out_file)?;
    }

    Ok(())
}

fn fragment(id: &str) -> String {
    format!("#{}", id)
}

fn check_if_elements_are_present(
    handle: &rsvg::Handle,
    elements: &Vec<String>,
) -> Result<(), ProcessingError> {
    for id in elements {
        let frag = fragment(id);
        if !handle.has_sub(&frag) {
            return Err(ProcessingError::ElementNotFound(frag));
        }
    }

    Ok(())
}

fn main() {
    let opt = Opt::from_args();

    if opt.elements.is_empty() {
        eprintln!("No element names specified\n");

        let app = Opt::clap();
        let mut out = io::stderr();
        app.write_help(&mut out).expect("failed to write to stderr");
        eprintln!("");
        process::exit(1);
    }

    match run(&opt) {
        Ok(_) => (),
        Err(e) => {
            eprintln!("{}", e);
            process::exit(1);
        }
    }
}
